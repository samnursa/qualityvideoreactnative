import React, {Component} from 'react';
import {StyleSheet, View, Button, Dimensions} from 'react-native';
import { Parser } from 'm3u8-parser';
import Video from 'react-native-video';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mute: false,
      pause: false,
      orientation : '',
      dataVideo:[],
      uriVideo:'http://118.97.167.8/jhos/tvone.m3u8?stime=20181031190000&etime=20181031210000&encoded=0e685fdfcfaba2854fc8d'
    };
  }

  componentDidMount()
  {
    this.loadData();
    this.getOrientation();
    Dimensions.addEventListener( 'change', () =>
    {
      this.getOrientation();
    });
  }

  //compare quality
  compare(a,b) {
    if (a.attributes.BANDWIDTH < b.attributes.BANDWIDTH)
      return -1;
    if (a.attributes.BANDWIDTH > b.attributes.BANDWIDTH)
      return 1;
    return 0;
  }

  //get data from backend api in m3u8 form
  loadData() {
    const {uriVideo} = this.state;
    fetch(uriVideo)
        .then(response => {
          const parser = new Parser();
          //parsing m3u8 to json
          parser.push(response._bodyText);
          parser.end();
          //sorting video by quality
          let temp = parser.manifest.playlists.sort(this.compare)
          this.setState({dataVideo:temp})
        })
        .catch((error) => {
            alert('Connection');
        })
  }

  //get orientation screen, so video can change resize manually
  getOrientation = () =>
  {
    if( this.refs.rootView )
    {
        if( Dimensions.get('window').width < Dimensions.get('window').height )
        {
          this.setState({ orientation: 'portrait' });
        }
        else
        {
          this.setState({ orientation: 'landscape' });
        }
    }
  }

  //muted or unmuted video
  mutedControl() {
    const { mute } = this.state;
    let temp = !mute
    this.setState({mute:temp})
  }

  //play or pause video
  pauseControl() {
    const { pause } = this.state;
    let temp = !pause
    this.setState({pause:temp})
  }

  setQuality(val){
    this.setState({uriVideo:val});
  }

  render() {
    const { mute, pause, orientation, dataVideo, uriVideo } = this.state;
    return (
      <View ref = "rootView" style={styles.container}>
        <Video source={{uri: uriVideo}}
        ref={(ref) => {
          this.player = ref
        }}                                      // Store reference
        paused={pause}
        muted={mute}
        resizeMode={orientation == 'portrait' ? 'contain' : 'cover'}
        onBuffer={this.onBuffer}                // Callback when remote video is buffering
        onError={this.videoError}               // Callback when video cannot be loaded             
        style={styles.backgroundVideo}/>
        <Button
          onPress={()=>{this.mutedControl()}}
          title="muted"
          color="#000"
        />
        <Button
          onPress={()=>{this.pauseControl()}}
          title="Play"
          color="#000"
        />
        {
            dataVideo.length > 0 ?
            dataVideo.map(data => {
              return <Button
                        title= {data.attributes.NAME}
                        onPress={()=>{this.setQuality(data.uri)}}
                        color="#000"
                        />
            }) : null
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});
